;(window['webpackJsonp'] = window['webpackJsonp'] || []).push([
  ['pages-read-read'],
  {
    '1b8a': function (t, e, a) {
      'use strict'
      a.r(e)
      var o = a('e4c3'),
        n = a.n(o)
      for (var i in o)
        'default' !== i &&
          (function (t) {
            a.d(e, t, function () {
              return o[t]
            })
          })(i)
      e['default'] = n.a
    },
    '1e69': function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADBklEQVRYR7WXMW7bQBBF59P20qVzgth9AMsnsG4Q5wSRGi/ZSSewdAN15LqR1aWLAqQ306WLcgLrBnFlQAS8E4yxBGiKa1GizEYAteC8nZ358xfkeZIk6QK4AdAhohPfuk3vmTlj5nEcx1ndWtS9NMYMiGhkrZXfLI7j5aZAxf9JkvSCIBgR0UApleV53nPf6tR9Zw0gSZLTIAgeAFxcX18vmgYu1qVpOgeQaa0npXcTAI9aawF79awBGGNGzHwSRZHsfuvHGJNZa0fllMs35UONANwOFnWLm9AYY5bM/I+IHkvrTwHMGgHIDuTcdwVI0/S3tfYbgOrxLRvVQFuA29vbztHR0bLf75cz4E1eXQ3slAEpXgCfAby0LDMvwjD8tQmkNYALPJWuIaI5Eb20LDNfAfhIRBOt9diXglYAkm5mvmfmWRiGo+punZhJO/6JoqjfSIia1sB0Oj3J8/zBWjuM4/jOt0NZt1qtMgDzvXaB6+2u1rr7VnsKwNPT06fDw8Of1to1Ndz5CKTfRW611nLutY/L0j0RzZi5y8zzarbaALDWunaWCE0p+F+tdc+nsK0BJFC1+KrBBch3ZK0AlFIf8jyXo5gqpcYCUhe8BLA2D9oAyNC5Oz4+nq9WK+mCcwBDIrohope0lwsjTdMFgFG1ZnYGkLkvhiUMwwvZuTHmiogERNrtVXDnL6RgT5uM48ZS7CYnK6X6PsktxMpa+6XOFe2cgaLSi/RLS0ZR9KPkjMTYfCWioTgrn1i1AigFk+MYADgnIhnDkmoZSjNnTryWbi8A5V274OQzoXutgTr5kwF0cHDw2NRPbp0BqXYAS1+ApsOsgN8KwKlZYVZ7dXPg3QCMMdLjsvvu8/NzJwiCSV11vwtAOXiRetffooavWmyvAE7Xp8x8FoZhtyo2BYTMAq21yLAMncZCJuu9NaCUmuR5LnYLdcGLIiogmPm72K69AIijBXC5KXgBUdgu8X4Azra5V/gycCkqppQabLLVVQinhuOmF5u93g1LBvSufDl9yzP+B+zRYj/GdqQLAAAAAElFTkSuQmCC'
    },
    '1f04': function (t, e, a) {
      'use strict'
      a.r(e)
      var o = a('f327'),
        n = a('1b8a')
      for (var i in n)
        'default' !== i &&
          (function (t) {
            a.d(e, t, function () {
              return n[t]
            })
          })(i)
      a('2a9d')
      var d,
        r = a('f0c5'),
        c = Object(r['a'])(n['default'], o['b'], o['c'], !1, null, '891c6f36', null, !1, o['a'], d)
      e['default'] = c.exports
    },
    '2a9d': function (t, e, a) {
      'use strict'
      var o = a('7983'),
        n = a.n(o)
      n.a
    },
    '30d2': function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAvElEQVRYR+3W3QmEMBAE4Nmeth/vKrAVS/A6ufdsD2cpIij45v6MyEF8jThfljhE8PAjD+ejA/5nAq21r4j8VPXNPDfuCZjZBGAEMDMRbsC2azObAQxMRAhwByIMYCNSACYiDWAhSgAGogyoIiiACoIGyCLuAnxU9eWpbBrg1JLu8A1IAWTDKYBKeBlQDS8BGOFpACs8BWCGhwHs8BDgdCUL/edXZeTugf1Sungb7ir4WHcDvB+MvtcBfQIr8yRuIQOvDPQAAAAASUVORK5CYII='
    },
    '334d': function (t, e, a) {
      'use strict'
      var o
      a.d(e, 'b', function () {
        return n
      }),
        a.d(e, 'c', function () {
          return i
        }),
        a.d(e, 'a', function () {
          return o
        })
      var n = function () {
          var t = this,
            e = t.$createElement,
            o = t._self._c || e
          return o(
            'v-uni-view',
            { staticClass: 'page', class: t.isHorizon ? 'is-horizon' : '' },
            [
              o('iheader', { staticClass: 'header', attrs: { title: t.book.book_name } }),
              o(
                'v-uni-view',
                {
                  class: 'bg-theme' + t.setting.themeIndex,
                  style: 'min-height:' + (t.sys.screenHeight - t.sys.statusBarHeight - 55) + 'px',
                  on: {
                    click: function (e) {
                      ;(arguments[0] = e = t.$handleEvent(e)), t.pageClick.apply(void 0, arguments)
                    },
                  },
                },
                [
                  o(
                    'v-uni-view',
                    {
                      class: 'markdown-body editormd-preview-container bg-theme' + t.setting.themeIndex,
                      style: 'line-height:1.8;font-size:' + t.fontIndexs[t.setting.fontIndex],
                    },
                    [
                      o('v-uni-view', { staticClass: 'title font-lv1 text-center' }, [t._v(t._s(t.article.title))]),
                      t._l(t.nodes, function (e, a) {
                        return [
                          'img' == e.type
                            ? [
                                o('v-uni-image', {
                                  attrs: { src: e['src'], 'data-src': e['src'], mode: 'aspectFit' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.imgPreview.apply(void 0, arguments)
                                    },
                                  },
                                }),
                              ]
                            : 'audio' == e.type
                            ? [
                                o('v-uni-audio', {
                                  attrs: { src: e['src'], poster: e['poster'], name: e['text'], controls: !0 },
                                }),
                              ]
                            : 'video' == e.type
                            ? [
                                o('v-uni-video', {
                                  attrs: { src: e['src'], poster: e['poster'], name: e['text'], controls: !0 },
                                }),
                              ]
                            : 'iframe' == e.type
                            ? [o('v-uni-web-view', { attrs: { src: e['src'] } })]
                            : 'richtext' == e.type
                            ? [o('v-uni-rich-text', { attrs: { nodes: e.data } })]
                            : t._e(),
                        ]
                      }),
                    ],
                    2
                  ),
                ],
                1
              ),
              o(
                'v-uni-view',
                { class: 'drawer drawer-left ' + [t.showMenu ? 'show' : ''] },
                [
                  o(
                    'v-uni-view',
                    { staticClass: 'drawer-content', style: 'padding-bottom: 70px;' + t.menuStyle },
                    [
                      o('imenu', {
                        attrs: {
                          book: t.book,
                          currentDocId: t.article.id,
                          wd: t.wd,
                          menu: t.menuTree,
                          result: t.result,
                          tips: t.tips,
                        },
                        on: {
                          search: function (e) {
                            ;(arguments[0] = e = t.$handleEvent(e)), t.search.apply(void 0, arguments)
                          },
                          clear: function (e) {
                            ;(arguments[0] = e = t.$handleEvent(e)), t.clear.apply(void 0, arguments)
                          },
                          itemClick: function (e) {
                            ;(arguments[0] = e = t.$handleEvent(e)), t.itemClick.apply(void 0, arguments)
                          },
                        },
                      }),
                    ],
                    1
                  ),
                ],
                1
              ),
              o(
                'v-uni-view',
                { class: 'drawer drawer-right ' + [t.showMore ? 'show' : ''] },
                [
                  o(
                    'v-uni-view',
                    { staticClass: 'drawer-content' },
                    [
                      o(
                        'v-uni-view',
                        { staticClass: 'more-setting', staticStyle: { bottom: '70px' } },
                        [
                          t.h5
                            ? t._e()
                            : [
                                o(
                                  'v-uni-view',
                                  { staticClass: 'row setting-tips' },
                                  [o('v-uni-text', { staticClass: 'color-grey font-lv4' }, [t._v('屏幕亮度')])],
                                  1
                                ),
                                o(
                                  'v-uni-view',
                                  { staticClass: 'row setting-screen setting-item ' },
                                  [
                                    o('v-uni-slider', {
                                      attrs: {
                                        min: '0',
                                        max: '1',
                                        step: '0.1',
                                        'block-size': '18',
                                        value: t.screenBrightness,
                                        'show-value': !0,
                                      },
                                      on: {
                                        change: function (e) {
                                          ;(arguments[0] = e = t.$handleEvent(e)),
                                            t.setBrightnessScreen.apply(void 0, arguments)
                                        },
                                      },
                                    }),
                                  ],
                                  1
                                ),
                              ],
                          o(
                            'v-uni-view',
                            { staticClass: 'row setting-tips' },
                            [o('v-uni-text', { staticClass: 'color-grey font-lv4' }, [t._v('字体大小')])],
                            1
                          ),
                          o(
                            'v-uni-view',
                            { staticClass: 'row setting-font setting-item' },
                            [
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col',
                                  attrs: { 'data-action': 'minus' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setFont.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [o('v-uni-image', { attrs: { src: '/static/images/font-minus.png' } })],
                                1
                              ),
                              o('v-uni-view', { staticClass: 'col' }, [t._v(t._s(t.setting.fontIndex + 1))]),
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col',
                                  attrs: { 'data-action': 'plus' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setFont.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [o('v-uni-image', { attrs: { src: '/static/images/font-plus.png' } })],
                                1
                              ),
                            ],
                            1
                          ),
                          o(
                            'v-uni-view',
                            { staticClass: 'row setting-tips' },
                            [o('v-uni-text', { staticClass: 'color-grey font-lv4' }, [t._v('阅读背景')])],
                            1
                          ),
                          o(
                            'v-uni-view',
                            { staticClass: 'row setting-bg setting-item' },
                            [
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col bg-theme0',
                                  attrs: { 'data-theme': '0' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setTheme.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [
                                  0 == t.setting.themeIndex
                                    ? o('v-uni-image', { attrs: { src: '/static/images/checked.png' } })
                                    : t._e(),
                                ],
                                1
                              ),
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col bg-theme1',
                                  attrs: { 'data-theme': '1' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setTheme.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [
                                  1 == t.setting.themeIndex
                                    ? o('v-uni-image', { attrs: { src: '/static/images/checked.png' } })
                                    : t._e(),
                                ],
                                1
                              ),
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col  bg-theme2',
                                  attrs: { 'data-theme': '2' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setTheme.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [
                                  2 == t.setting.themeIndex
                                    ? o('v-uni-image', { attrs: { src: '/static/images/checked.png' } })
                                    : t._e(),
                                ],
                                1
                              ),
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col  bg-theme3',
                                  attrs: { 'data-theme': '3' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setTheme.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [
                                  3 == t.setting.themeIndex
                                    ? o('v-uni-image', { attrs: { src: '/static/images/checked.png' } })
                                    : t._e(),
                                ],
                                1
                              ),
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col  bg-theme4',
                                  attrs: { 'data-theme': '4' },
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.setTheme.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [
                                  4 == t.setting.themeIndex
                                    ? o('v-uni-image', { attrs: { src: '/static/images/checked.png' } })
                                    : t._e(),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          o(
                            'v-uni-view',
                            { staticClass: 'row setting-btn setting-item color-grey font-lv3' },
                            [
                              o(
                                'v-uni-view',
                                {
                                  staticClass: 'col',
                                  on: {
                                    click: function (e) {
                                      ;(arguments[0] = e = t.$handleEvent(e)), t.resetSetting.apply(void 0, arguments)
                                    },
                                  },
                                },
                                [o('v-uni-text', [t._v('恢复默认值')])],
                                1
                              ),
                              o(
                                'v-uni-navigator',
                                {
                                  staticClass: 'col',
                                  attrs: { url: '/pages/bookmarks/bookmarks?identify=' + t.book.book_id },
                                },
                                [o('v-uni-text', [t._v('查看书签')])],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        2
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
              o(
                'v-uni-view',
                { staticClass: 'footer' },
                [
                  o(
                    'v-uni-view',
                    { staticClass: 'row font-lv3' },
                    [
                      t.article.bookmark
                        ? o(
                            'v-uni-view',
                            {
                              staticClass: 'col',
                              attrs: { 'data-action': 'cancel' },
                              on: {
                                click: function (e) {
                                  ;(arguments[0] = e = t.$handleEvent(e)), t.clickBookmark.apply(void 0, arguments)
                                },
                              },
                            },
                            [o('v-uni-image', { attrs: { src: a('1e69') } })],
                            1
                          )
                        : o(
                            'v-uni-view',
                            {
                              staticClass: 'col',
                              attrs: { 'data-action': 'add' },
                              on: {
                                click: function (e) {
                                  ;(arguments[0] = e = t.$handleEvent(e)), t.clickBookmark.apply(void 0, arguments)
                                },
                              },
                            },
                            [o('v-uni-image', { attrs: { src: a('eea3') } })],
                            1
                          ),
                      t.preDisable
                        ? o('v-uni-view', { staticClass: 'col' }, [o('v-uni-image', { attrs: { src: a('80d0') } })], 1)
                        : o(
                            'v-uni-view',
                            {
                              staticClass: 'col',
                              on: {
                                click: function (e) {
                                  ;(arguments[0] = e = t.$handleEvent(e)), t.clickPrev.apply(void 0, arguments)
                                },
                              },
                            },
                            [o('v-uni-image', { attrs: { src: a('4a87') } })],
                            1
                          ),
                      o(
                        'v-uni-view',
                        {
                          staticClass: 'col',
                          on: {
                            click: function (e) {
                              ;(arguments[0] = e = t.$handleEvent(e)), t.clickMenu.apply(void 0, arguments)
                            },
                          },
                        },
                        [o('v-uni-image', { attrs: { src: a('e4ce') } })],
                        1
                      ),
                      t.nextDisable
                        ? o('v-uni-view', { staticClass: 'col' }, [o('v-uni-image', { attrs: { src: a('30d2') } })], 1)
                        : o(
                            'v-uni-view',
                            {
                              staticClass: 'col',
                              on: {
                                click: function (e) {
                                  ;(arguments[0] = e = t.$handleEvent(e)), t.clickNext.apply(void 0, arguments)
                                },
                              },
                            },
                            [o('v-uni-image', { attrs: { src: a('c173') } })],
                            1
                          ),
                      o(
                        'v-uni-view',
                        {
                          staticClass: 'col',
                          on: {
                            click: function (e) {
                              ;(arguments[0] = e = t.$handleEvent(e)), t.clickMore.apply(void 0, arguments)
                            },
                          },
                        },
                        [o('v-uni-image', { attrs: { src: a('de52') } })],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          )
        },
        i = []
    },
    '38cf': function (t, e, a) {
      var o = a('23e7'),
        n = a('1148')
      o({ target: 'String', proto: !0 }, { repeat: n })
    },
    '4a87': function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxUlEQVRYR+3WwQ3CMAwF0O9J6CgwCbABR3uCnNmgnTQoUishLo3tTytQe7b6X9zqK4KdH9k5Hwfg/zZQShlF5KSql57/i7qBOfwK4Kmqj00BS3itdTKzW094m6FsIBpOAWTC04BseArACA8DWOEhADPcDWCHuwDfCA8BAIyqeu8tmrU5VxG9VS0N4QK007ARbgAbEQIwEWEAC5ECMBBpQBZBAWQQNMAHYvsr2dJ4rScADGZ2XmtBVxX3vCwyQ/0EB+AnN/ACdQuHIX8BfwUAAAAASUVORK5CYII='
    },
    '52b1': function (t, e, a) {
      'use strict'
      var o = a('4ea4')
      a('4de4'),
        a('c975'),
        a('a9e3'),
        a('b680'),
        a('d3b7'),
        a('acd8'),
        a('ac1f'),
        a('3ca3'),
        a('1276'),
        a('ddb0'),
        Object.defineProperty(e, '__esModule', { value: !0 }),
        (e.default = void 0)
      var n = o(a('3835')),
        i = o(a('9d99')),
        d = (o(a('d0c3')), o(a('6ab1'))),
        r = o(a('2129')),
        c = o(a('3456')),
        l = o(a('1f04')),
        s = {
          components: { imenu: r.default, iheader: c.default, imtAudio: l.default },
          data: function () {
            return {
              isHorizon: !1,
              book: {},
              article: {},
              menuSortIds: [],
              nodes: [],
              menuTree: [],
              menuIndex: 0,
              bookmark: [],
              showMenu: !1,
              showMore: !1,
              preDisable: !1,
              nextDisable: !1,
              identify: '',
              menuStyle: '',
              wd: '',
              audios: [],
              setting: { themeIndex: 0, fontIndex: 0 },
              defautScreenBrightness: 0,
              screenBrightness: 0,
              fontIndexs:
                i.default.getSysInfo().windowWidth >= 768
                  ? ['15px', '16px', '17px', '18px', '19px', '20px', '21px']
                  : ['14px', '15px', '16px', '17px', '18px', '19px', '20px'],
              tips: '',
              result: [],
              h5: !1,
              sys: i.default.getSysInfo(),
            }
          },
          onLoad: function (t) {
            ;(this.changeOrien = this.changeOrien.bind(this)),
              window && window.addEventListener('orientationchange', this.changeOrien, !1),
              this.changeOrien()
            var e = this,
              a = t.identify || 'help',
              o = String(a).split('/'),
              r = {},
              c = []
            if (((e.h5 = !0), 0 != o.length)) {
              e.initReaderSetting()
              var l = 0
              Promise.all([
                i.default.request(d.default.api.bookMenu, { identify: o[0] }),
                i.default.request(d.default.api.bookInfo, { identify: o[0] }),
              ])
                .then(function (t) {
                  var e = (0, n.default)(t, 2),
                    a = e[0],
                    o = e[1]
                  d.default.debug && console.log(a, o),
                    a.data && a.data.menu && (c = a.data.menu),
                    a.data && a.data.latest_read_id && (l = a.data.latest_read_id),
                    o.data &&
                      o.data.book &&
                      ((r = o.data.book),
                      (r.score_float = Number(r.score / 10).toFixed(1)),
                      (r.is_read = 1),
                      (r.percent = Number((r.cnt_readed / r.cnt_doc) * 100).toFixed(2)))
                })
                .catch(function (t) {
                  console.log(t)
                })
                .finally(function () {
                  if (0 != c.length) {
                    var t = i.default.menuToTree(c),
                      n = i.default.getSysInfo(),
                      s = n.titleBarHeight + n.statusBarHeight
                    ;(e.menuStyle = 'padding-top: '.concat(s, 'px;')),
                      (e.menuSortIds = i.default.menuSortIds(t)),
                      (e.menuTree = t),
                      (e.book = r),
                      2 != o.length && (a = l > 0 ? r.book_id + '/' + l : r.book_id + '/' + t[0].id),
                      d.default.debug && console.log('sys info', i.default.getSysInfo()),
                      e.getArticle(a)
                  } else uni.redirectTo({ url: '/pages/notfound/notfound' })
                })
            } else uni.redirectTo({ url: '/pages/notfound/notfound' })
          },
          onUnload: function () {
            window && window.removeEventListener('orientationchange', this.changeOrien), uni.hideLoading()
          },
          onShareAppMessage: function () {
            uni.showShareMenu({ withShareTicket: !0 })
          },
          methods: {
            changeOrien: function () {
              ;(180 !== window.orientation && 0 !== window.orientation) || (this.isHorizon = !1),
                (90 !== window.orientation && -90 !== window.orientation) || (this.isHorizon = !0)
            },
            getArticle: function (t) {
              i.default.loading('loading...')
              var e = {},
                a = this,
                o = { identify: t, 'enhance-richtext': !0 }
              i.default
                .request(d.default.api.read, o)
                .then(function (t) {
                  t.data && t.data.article && (e = t.data.article)
                })
                .catch(function (t) {
                  var e = t.data.message || t.errMsg
                  i.default.toastError(e)
                })
                .finally(function () {
                  var o = a.menuSortIds.indexOf(e.id) + 1 == a.menuSortIds.length,
                    n = 0 == a.menuSortIds.indexOf(e.id)
                  e.content || (e.content = []),
                    (a.nodes = []),
                    (a.article = e),
                    (a.identify = t),
                    (a.showMenu = !1),
                    (a.showMore = !1),
                    (a.nextDisable = o),
                    (a.preDisable = n),
                    (a.menuTree = i.default.menuTreeReaded(a.menuTree, e.id)),
                    setTimeout(function () {
                      ;(a.nodes = e.content.filter(function (t, e) {
                        if ('img' == t.type || 'iframe' == t.type)
                          try {
                            t['src'] = t.data[0].attrs['src']
                          } catch (a) {
                            return console.log(a), !1
                          }
                        else if ('audio' == t.type || 'video' == t.type)
                          try {
                            ;(t['src'] = t.data[0].attrs['src']),
                              (t['poster'] = t.data[0].attrs['poster']),
                              (t['text'] = t.data[0].children[0]['text']),
                              (t['id'] = 'id' + e)
                          } catch (a) {
                            return console.log(a), !1
                          }
                        return !0
                      })),
                        uni.pageScrollTo({ scrollTop: 0, duration: 100 }),
                        uni.hideLoading()
                    }, 10)
                })
            },
            pageClick: function (t) {
              d.default.debug && console.log('contentClick', t), (this.showMenu = !1), (this.showMore = !1)
            },
            clickMenu: function (t) {
              ;(this.showMenu = !this.showMenu), (this.showMore = !1)
            },
            clickMore: function (t) {
              ;(this.showMore = !this.showMore), (this.showMenu = !1)
            },
            imgPreview: function (t) {
              d.default.debug && console.log('imgPreview', t), uni.previewImage({ urls: [t.currentTarget.dataset.src] })
            },
            clickNext: function () {
              var t = this,
                e = t.menuSortIds.indexOf(t.article.id)
              e++,
                (t.nextDisable = !0),
                e < t.menuSortIds.length
                  ? t.getArticle(t.book.book_id + '/' + t.menuSortIds[e])
                  : uni.showToast({ title: '没有下一章节了', mask: !0, image: '/static/images/error.png' })
            },
            clickPrev: function () {
              var t = this,
                e = t.menuSortIds.indexOf(t.article.id)
              ;(t.preDisable = !0),
                e--,
                e > -1
                  ? t.getArticle(t.book.book_id + '/' + t.menuSortIds[e])
                  : uni.showToast({ title: '没有上一章节了', mask: !0, image: '/static/images/error.png' })
            },
            itemClick: function (t) {
              this.getArticle(t.identify)
            },
            search: function (t) {
              d.default.debug && console.log('search', t)
              var e = this,
                a = []
              i.default.loading('玩命搜索中...'),
                i.default
                  .request(d.default.api.searchDoc, { identify: e.book.book_id, wd: t.wd })
                  .then(function (t) {
                    d.default.debug && console.log(d.default.api.searchDoc, t),
                      t.data && t.data.result && (a = t.data.result)
                  })
                  .catch(function (t) {
                    console.log(t)
                  })
                  .finally(function () {
                    uni.hideLoading(),
                      (e.result = a),
                      (e.wd = t.wd),
                      0 == a.length && i.default.toastError('没有搜索到结果')
                  })
            },
            clear: function (t) {
              this.result = []
            },
            clickBookmark: function (t) {
              var e = this
              '' == i.default.getToken()
                ? uni.showModal({
                    title: '温馨提示',
                    content: '您当前未登录，无法添加书签，是否要跳转登录？',
                    success: function (t) {
                      t.confirm &&
                        uni.navigateTo({
                          url:
                            '/pages/login/login?redirect=' +
                            encodeURIComponent('/pages/read/read?identify=' + e.book.book_id + '/' + e.article.id),
                        })
                    },
                  })
                : 'cancel' == t.currentTarget.dataset.action
                ? uni.showModal({
                    title: '温馨提示',
                    content: '您确定要取消该书签吗？',
                    success: function (t) {
                      t.confirm && e._clickBookmark('cancel')
                    },
                  })
                : e._clickBookmark('add')
            },
            renderContent: function (t) {
              var e = this,
                a = setTimeout(function () {
                  ;(e.nodes = t), clearTimeout(a)
                }, 300)
            },
            setFont: function (t) {
              d.default.debug && console.log(t)
              var e = this,
                a = e.setting
              'minus' == t.currentTarget.dataset.action
                ? a.fontIndex > 0 && (a.fontIndex = a.fontIndex - 1)
                : a.fontIndex < 6 && (a.fontIndex = a.fontIndex + 1),
                (e.setting = a),
                i.default.setReaderSetting(Object(a))
            },
            setTheme: function (t) {
              d.default.debug && console.log(t)
              var e = this,
                a = e.setting
              t.currentTarget.dataset.theme >= 0 && t.currentTarget.dataset.theme < 5
                ? (a.themeIndex = t.currentTarget.dataset.theme)
                : (a.themeIndex = 0),
                (e.setting = a),
                i.default.setReaderSetting(Object(a))
            },
            setBrightnessScreen: function (t) {
              d.default.debug && console.log(t)
              var e = parseFloat(t.detail.value).toFixed(1)
              ;(this.screenBrightness = e), uni.setScreenBrightness({ value: e })
            },
            initReaderSetting: function () {
              var t = i.default.getReaderSetting(),
                e = 0
              ;(this.setting = t), (this.defautScreenBrightness = e), (this.screenBrightness = e)
            },
            resetSetting: function () {
              var t = { fontIndex: 0, themeIndex: 0 }
              ;(this.setting = t),
                (this.screenBrightness = this.defautScreenBrightness),
                uni.setScreenBrightness({ value: this.defautScreenBrightness }),
                i.default.setReaderSetting(t)
            },
            _clickBookmark: function (t) {
              var e = this,
                a = this.article,
                o = 'cancel' == t ? 'DELETE' : 'PUT'
              i.default
                .request(d.default.api.bookmark + '?doc_id=' + a.id, {}, o)
                .then(function (o) {
                  d.default.debug && console.log(d.default.api.bookmark + '?doc_id=' + a.id, o),
                    (a.bookmark = !a.bookmark),
                    (e.article = a),
                    uni.showToast({ title: 'cancel' == t ? '移除书签成功' : '添加书签成功' })
                })
                .catch(function (t) {
                  i.default.toastError(t.data.message || t.errMsg)
                })
            },
          },
        }
      e.default = s
    },
    5481: function (t, e, a) {
      var o = a('24fb')
      ;(e = o(!1)),
        e.push([
          t.i,
          '@font-face{font-family:icon;src:url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.eot);src:url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.eot#iefix) format("embedded-opentype"),url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.woff2) format("woff2"),url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.woff) format("woff"),url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.ttf) format("truetype"),url(https://at.alicdn.com/t/font_1104838_fxzimc34xw.svg#iconfont) format("svg")}.audio-box[data-v-891c6f36]{border:1px solid #e6e6e6;box-sizing:border-box;height:60px;padding-right:8px;border-radius:3px;overflow:hidden;margin:5px 0}.audio-poster[data-v-891c6f36]{width:60px;height:60px;margin-right:10px;float:left;background-color:#e6e6e6;box-sizing:border-box}.audio-wrapper[data-v-891c6f36]{display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-align:center;-webkit-align-items:center;align-items:center;margin-top:5px}.audio-number[data-v-891c6f36]{width:50px;font-size:12px;line-height:1;color:#333}.audio-slider[data-v-891c6f36]{-webkit-box-flex:1;-webkit-flex:1;flex:1;margin:0}.audio-title[data-v-891c6f36]{margin-top:5px;text-overflow:ellipsis;display:-webkit-box;-webkit-line-clamp:1;-webkit-box-orient:vertical;overflow:hidden}.audio-control-wrapper[data-v-891c6f36]{margin-top:9px;display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;align-items:center;font-family:icon!important}.audio-control[data-v-891c6f36]{font-size:15px;line-height:1;border:2px solid;border-radius:50%;padding:8px}.audio-control-next[data-v-891c6f36]{-webkit-transform:rotate(180deg);transform:rotate(180deg)}.audio-control-switch[data-v-891c6f36]{font-size:%?40?%;margin:0 %?100?%}.audioLoading[data-v-891c6f36]{-webkit-animation:loading-data-v-891c6f36 2s;animation:loading-data-v-891c6f36 2s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-timing-function:linear;animation-timing-function:linear}@-webkit-keyframes loading-data-v-891c6f36{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes loading-data-v-891c6f36{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}',
          '',
        ]),
        (t.exports = e)
    },
    6559: function (t, e, a) {
      'use strict'
      a.r(e)
      var o = a('334d'),
        n = a('b63c')
      for (var i in n)
        'default' !== i &&
          (function (t) {
            a.d(e, t, function () {
              return n[t]
            })
          })(i)
      a('a6f1')
      var d,
        r = a('f0c5'),
        c = Object(r['a'])(n['default'], o['b'], o['c'], !1, null, '9e73065c', null, !1, o['a'], d)
      e['default'] = c.exports
    },
    7983: function (t, e, a) {
      var o = a('5481')
      'string' === typeof o && (o = [[t.i, o, '']]), o.locals && (t.exports = o.locals)
      var n = a('4f06').default
      n('0797a6ee', o, !0, { sourceMap: !1, shadowMode: !1 })
    },
    '80d0': function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAwUlEQVRYR+3WwQ2DMAyFYWcSGOLlTieh3aCrdAPYJzsFRaIS6iWx/QBRwTni/zDIIsjJVzi5Lzfg/yaQUppEpAPwaPm+qBNY46OIfAC8DwVs4jOAZ0u8nKFMwBqnADxxN8AbdwEYcTOAFTcBmHE1gB1XAfaIWwETgFfroqmdUy2izRRoCBWgPA0boQawESYAE2EGsBAuAAPhBngRFIAHQQP8II7/JftuvLIncs59jHGobUHVKm65meUM9RXcgEtOYAGBrW8h4VLnmQAAAABJRU5ErkJggg=='
    },
    '88b4': function (t, e, a) {
      var o = a('9aaf')
      'string' === typeof o && (o = [[t.i, o, '']]), o.locals && (t.exports = o.locals)
      var n = a('4f06').default
      n('7c3a68a2', o, !0, { sourceMap: !1, shadowMode: !1 })
    },
    '9aaf': function (t, e, a) {
      var o = a('24fb')
      ;(e = o(!1)),
        e.push([
          t.i,
          '.tag-pre[data-v-9e73065c]{display:block;font-family:monospace;white-space:pre;margin:1em 0}.markdown-body[data-v-9e73065c]{color:#555;overflow:auto;word-wrap:break-word;-webkit-overflow-scrolling:touch}.tag-strong[data-v-9e73065c]{font-weight:400}.tag-img[data-v-9e73065c]{max-width:100%}.markdown-body .tag-img[data-v-9e73065c]{max-width:100%;box-sizing:border-box}.markdown-body .tag-hr[data-v-9e73065c]{box-sizing:initial;height:0}.markdown-body .tag-pre[data-v-9e73065c]{overflow:auto}.markdown-body .tag-table[data-v-9e73065c]{border-collapse:collapse;border-spacing:0}.markdown-body .tag-td[data-v-9e73065c],\n.markdown-body .tag-th[data-v-9e73065c]{padding:0}.markdown-body[data-v-9e73065c]{width:100%;box-sizing:border-box;padding:15px}.markdown-body .tag-a[data-v-9e73065c]{color:#4183c4;text-decoration:none}.markdown-body .tag-a[data-v-9e73065c]:hover,\n.markdown-body .tag-a[data-v-9e73065c]:active{text-decoration:none}.markdown-body .tag-hr[data-v-9e73065c]{height:0;margin:8px 0;overflow:hidden;background:transparent;border:0;border-bottom:1px solid #eee}.markdown-body .tag-hr[data-v-9e73065c]:before{display:table;content:""}.markdown-body .tag-hr[data-v-9e73065c]:after{display:table;clear:both;content:""}.markdown-body .tag-h1[data-v-9e73065c],\n.markdown-body .tag-h2[data-v-9e73065c],\n.markdown-body .tag-h3[data-v-9e73065c],\n.markdown-body .tag-h4[data-v-9e73065c],\n.markdown-body .tag-h5[data-v-9e73065c],\n.markdown-body .tag-h6[data-v-9e73065c]{margin-top:15px;margin-bottom:15px;font-weight:400}.markdown-body .tag-h1[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.6em!important}.markdown-body .tag-h2[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.5em!important}.markdown-body .tag-h3[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.4em!important}.markdown-body .tag-h4[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.3em!important}.markdown-body .tag-h5[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.2em!important}.markdown-body .tag-h6[data-v-9e73065c],\n.markdown-body .tag-strong[data-v-9e73065c]{font-size:1.1em!important}.markdown-body .tag-blockquote[data-v-9e73065c]{margin:0}.markdown-body .tag-ul[data-v-9e73065c],\n.markdown-body .tag-ol[data-v-9e73065c]{padding:0;margin-top:0;margin-bottom:0}.markdown-body .tag-ol .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ul .tag-ol[data-v-9e73065c]{list-style-type:lower-roman}.markdown-body .tag-ul .tag-ul .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ul .tag-ol .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ol .tag-ul .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ol .tag-ol .tag-ol[data-v-9e73065c]{list-style-type:lower-alpha}.markdown-body .tag-dd[data-v-9e73065c]{margin-left:0}.markdown-body .tag-pre[data-v-9e73065c]{margin-top:0;margin-bottom:0}.markdown-body .octicon[data-v-9e73065c]{font:normal normal 16px octicons-anchor;display:inline-block;text-decoration:none;}.markdown-body .anchor[data-v-9e73065c]{position:absolute;top:0;left:0;display:block;padding-right:6px;padding-left:30px;margin-left:-30px}.markdown-body .anchor[data-v-9e73065c]:focus{outline:none}.markdown-body .tag-h1[data-v-9e73065c],\n.markdown-body .tag-h2[data-v-9e73065c],\n.markdown-body .tag-h3[data-v-9e73065c],\n.markdown-body .tag-h4[data-v-9e73065c],\n.markdown-body .tag-h5[data-v-9e73065c],\n.markdown-body .tag-h6[data-v-9e73065c]{position:relative;font-weight:400}.markdown-body .tag-h1 .octicon-link[data-v-9e73065c],\n.markdown-body .tag-h2 .octicon-link[data-v-9e73065c],\n.markdown-body .tag-h3 .octicon-link[data-v-9e73065c],\n.markdown-body .tag-h4 .octicon-link[data-v-9e73065c],\n.markdown-body .tag-h5 .octicon-link[data-v-9e73065c],\n.markdown-body .tag-h6 .octicon-link[data-v-9e73065c]{display:none;color:#353535;vertical-align:middle}.markdown-body .tag-p[data-v-9e73065c],\n.markdown-body .tag-blockquote[data-v-9e73065c],\n.markdown-body .tag-ul[data-v-9e73065c],\n.markdown-body .tag-ol[data-v-9e73065c],\n.markdown-body .tag-dl[data-v-9e73065c],\n.markdown-body .tag-table[data-v-9e73065c],\n.markdown-body .tag-pre[data-v-9e73065c]{margin-top:0;margin-bottom:16px}.markdown-body .tag-ul[data-v-9e73065c],\n.markdown-body .tag-ol[data-v-9e73065c]{padding-left:2em}.markdown-body .tag-ul .tag-ul[data-v-9e73065c],\n.markdown-body .tag-ul .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ol .tag-ol[data-v-9e73065c],\n.markdown-body .tag-ol .tag-ul[data-v-9e73065c]{margin-top:0;margin-bottom:0}.markdown-body .tag-dl[data-v-9e73065c]{padding:0}.markdown-body .tag-dl .tag-dt[data-v-9e73065c]{padding:0;font-size:1em;font-style:normal;font-weight:700}.markdown-body .tag-dl .tag-dd[data-v-9e73065c]{padding:0 16px;margin-bottom:16px}.markdown-body .tag-blockquote[data-v-9e73065c]{padding:0 15px;color:#777;border-left:4px solid #ddd}.markdown-body .tag-table[data-v-9e73065c]{display:block;width:100%;overflow:auto;word-break:keep-all}.markdown-body .tag-table .tag-th[data-v-9e73065c]{font-weight:700}.markdown-body .tag-table .tag-th[data-v-9e73065c],\n.markdown-body .tag-table .tag-td[data-v-9e73065c]{padding:6px 13px;border:1px solid #eee}.markdown-body .tag-table .tag-tr[data-v-9e73065c]{background-color:#fff;border-top:1px solid #ddd}.markdown-body .tag-code[data-v-9e73065c]{padding-top:.2em;padding-bottom:.2em;margin:0;background-color:rgba(0,0,0,.04);border-radius:3px}.markdown-body .tag-code[data-v-9e73065c]:before,\n.markdown-body .tag-code[data-v-9e73065c]:after{letter-spacing:-.2em;content:"\\00a0"}.markdown-body .tag-pre>.tag-code[data-v-9e73065c]{padding:0;margin:0;font-size:100%;word-break:normal;white-space:pre;background:transparent;border:0}.markdown-body .highlight[data-v-9e73065c]{margin-bottom:16px}.markdown-body .highlight .tag-pre[data-v-9e73065c],\n.markdown-body .tag-pre[data-v-9e73065c]{padding:16px;overflow:auto;\n\t/* font-size: 85%; */background-color:#f7f7f7;border-radius:3px}.markdown-body .highlight .tag-pre[data-v-9e73065c]{margin-bottom:0;word-break:normal}.markdown-body .tag-pre[data-v-9e73065c]{word-wrap:normal}.markdown-body .tag-pre .tag-code[data-v-9e73065c]{display:inline;max-width:none;padding:0;margin:0;overflow:initial;line-height:inherit;word-wrap:normal;background-color:initial;border:0}.markdown-body .tag-pre .tag-code[data-v-9e73065c]:before,\n.markdown-body .tag-pre .tag-code[data-v-9e73065c]:after{content:normal}.markdown-body .tag-kbd[data-v-9e73065c]{display:inline-block;padding:3px 5px;\n\t/* font-size: 11px; */color:#555;vertical-align:middle;background-color:#fcfcfc;border:solid 1px #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb}.markdown-body .pl-c[data-v-9e73065c]{color:#969896}.markdown-body .pl-c1[data-v-9e73065c],\n.markdown-body .pl-mdh[data-v-9e73065c],\n.markdown-body .pl-mm[data-v-9e73065c],\n.markdown-body .pl-mp[data-v-9e73065c],\n.markdown-body .pl-mr[data-v-9e73065c],\n.markdown-body .pl-s1 .pl-v[data-v-9e73065c],\n.markdown-body .pl-s3[data-v-9e73065c],\n.markdown-body .pl-sc[data-v-9e73065c],\n.markdown-body .pl-sv[data-v-9e73065c]{color:#0086b3}.markdown-body .pl-e[data-v-9e73065c],\n.markdown-body .pl-en[data-v-9e73065c]{color:#795da3}.markdown-body .pl-s1 .pl-s2[data-v-9e73065c],\n.markdown-body .pl-smi[data-v-9e73065c],\n.markdown-body .pl-smp[data-v-9e73065c],\n.markdown-body .pl-stj[data-v-9e73065c],\n.markdown-body .pl-vo[data-v-9e73065c],\n.markdown-body .pl-vpf[data-v-9e73065c]{color:#333}.markdown-body .pl-ent[data-v-9e73065c]{color:#63a35c}.markdown-body .pl-k[data-v-9e73065c],\n.markdown-body .pl-s[data-v-9e73065c],\n.markdown-body .pl-st[data-v-9e73065c]{color:#a71d5d}.markdown-body .pl-pds[data-v-9e73065c],\n.markdown-body .pl-s1[data-v-9e73065c],\n.markdown-body .pl-s1 .pl-pse .pl-s2[data-v-9e73065c],\n.markdown-body .pl-sr[data-v-9e73065c],\n.markdown-body .pl-sr .pl-cce[data-v-9e73065c],\n.markdown-body .pl-sr .pl-sra[data-v-9e73065c],\n.markdown-body .pl-sr .pl-sre[data-v-9e73065c],\n.markdown-body .pl-src[data-v-9e73065c]{color:#df5000}.markdown-body .pl-mo[data-v-9e73065c],\n.markdown-body .pl-v[data-v-9e73065c]{color:#1d3e81}.markdown-body .pl-id[data-v-9e73065c]{color:#b52a1d}.markdown-body .pl-ii[data-v-9e73065c]{background-color:#b52a1d;color:#f8f8f8}.markdown-body .pl-sr .pl-cce[data-v-9e73065c]{color:#63a35c;font-weight:700}.markdown-body .pl-ml[data-v-9e73065c]{color:#693a17}.markdown-body .pl-mh[data-v-9e73065c],\n.markdown-body .pl-mh .pl-en[data-v-9e73065c],\n.markdown-body .pl-ms[data-v-9e73065c]{color:#1d3e81;font-weight:700}.markdown-body .pl-mq[data-v-9e73065c]{color:teal}.markdown-body .pl-mi[data-v-9e73065c]{color:#333;font-style:italic}.markdown-body .pl-mb[data-v-9e73065c]{color:#333;font-weight:700}.markdown-body .pl-md[data-v-9e73065c],\n.markdown-body .pl-mdhf[data-v-9e73065c]{background-color:#ffecec;color:#bd2c00}.markdown-body .pl-mdht[data-v-9e73065c],\n.markdown-body .pl-mi1[data-v-9e73065c]{background-color:#eaffea;color:#55a532}.markdown-body .pl-mdr[data-v-9e73065c]{color:#795da3;font-weight:700}.markdown-body .tag-kbd[data-v-9e73065c]{display:inline-block;padding:3px 5px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:solid 1px #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb}.markdown-body .task-list-item[data-v-9e73065c]{list-style-type:none}.markdown-body .task-list-item+.task-list-item[data-v-9e73065c]{margin-top:3px}.editormd-preview-container[data-v-9e73065c],\n.editormd-html-preview[data-v-9e73065c]{text-align:left;padding:20px;overflow:auto;width:100%;background-color:#fff;-webkit-overflow-scrolling:touch}.editormd-preview-container .tag-blockquote[data-v-9e73065c],\n.editormd-html-preview .tag-blockquote[data-v-9e73065c]{color:#666;border-left:4px solid #ddd;padding-left:20px;margin-left:0\n\t/* font-size: 14px; */}.editormd-preview-container .tag-p .tag-code[data-v-9e73065c],\n.editormd-html-preview .tag-p .tag-code[data-v-9e73065c]{margin-left:5px;margin-right:4px}.editormd-preview-container .tag-abbr[data-v-9e73065c],\n.editormd-html-preview .tag-abbr[data-v-9e73065c]{background:#ffd}.editormd-preview-container .tag-hr[data-v-9e73065c],\n.editormd-html-preview .tag-hr[data-v-9e73065c]{height:1px;border:none;border-top:1px solid #ddd;background:none}.editormd-preview-container .tag-code[data-v-9e73065c],\n.editormd-html-preview .tag-code[data-v-9e73065c]{border:1px solid #ddd;background:#f6f6f6;padding:3px;border-radius:3px\n\t/* font-size: 14px; */}.editormd-preview-container .tag-pre[data-v-9e73065c],\n.editormd-html-preview .tag-pre[data-v-9e73065c]{border:1px solid #ddd;background:#f6f6f6;padding:16px 16px;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px}.editormd-preview-container .tag-pre .tag-code[data-v-9e73065c],\n.editormd-html-preview .tag-pre .tag-code[data-v-9e73065c]{padding:0}.editormd-preview-container .tag-pre[data-v-9e73065c],\n.editormd-preview-container .tag-code[data-v-9e73065c],\n.editormd-preview-container kbd[data-v-9e73065c],\n.editormd-html-preview .tag-pre[data-v-9e73065c],\n.editormd-preview-container .tag-table .tag-thead .tag-tr[data-v-9e73065c],\n.editormd-html-preview .tag-table .tag-thead .tag-tr[data-v-9e73065c]{background-color:#f8f8f8}.editormd-preview-container .tag-p.editormd-tex[data-v-9e73065c],\n.editormd-html-preview .tag-p.editormd-tex[data-v-9e73065c]{text-align:center}.editormd-preview-container .tag-span.editormd-tex[data-v-9e73065c],\n.editormd-html-preview span.editormd-tex[data-v-9e73065c]{margin:0 5px}.editormd-preview-container .emoji[data-v-9e73065c],\n.editormd-html-preview .emoji[data-v-9e73065c]{width:24px;height:24px}.editormd-preview-container .sequence-diagram[data-v-9e73065c],\n.editormd-preview-container .flowchart[data-v-9e73065c],\n.editormd-html-preview .sequence-diagram[data-v-9e73065c],\n.editormd-html-preview .flowchart[data-v-9e73065c]{margin:0 auto;text-align:center}.editormd-preview-container .sequence-diagram .tag-svg[data-v-9e73065c],\n.editormd-preview-container .flowchart .tag-svg[data-v-9e73065c],\n.editormd-html-preview .sequence-diagram .tag-svg[data-v-9e73065c],\n.editormd-html-preview .flowchart .tag-svg[data-v-9e73065c]{margin:0 auto}.pln[data-v-9e73065c]{color:#000}\n\n/* plain text */.str[data-v-9e73065c]{color:#080}\n\n/* string content */.kwd[data-v-9e73065c]{color:#008}\n\n/* a keyword */.com[data-v-9e73065c]{color:#800}\n\n/* a comment */.typ[data-v-9e73065c]{color:#606}\n\n/* a type name */.lit[data-v-9e73065c]{color:#066}\n\n/* a literal value */\n\n/* punctuation, lisp open bracket, lisp close bracket */.pun[data-v-9e73065c],\n.opn[data-v-9e73065c],\n.clo[data-v-9e73065c]{color:#660}.tag[data-v-9e73065c]{color:#008}\n\n/* a markup tag name */.atn[data-v-9e73065c]{color:#606}\n\n/* a markup attribute name */.atv[data-v-9e73065c]{color:#080}\n\n/* a markup attribute value */.dec[data-v-9e73065c],\n.var[data-v-9e73065c]{color:#606}\n\n/* a declaration; a variable name */.fun[data-v-9e73065c]{color:red}\n\n/* a function name */\n\n/* Put a border around .tag-prettyprinted .tag-code snippets. */.tag-pre.prettyprint[data-v-9e73065c]{padding:2px;border:1px solid #888}\n\n/* Specify class=linenums on a .tag-pre to get line numbering */.tag-ol.linenums[data-v-9e73065c]{margin-top:0;margin-bottom:0}.editormd-preview-container .tag-pre.prettyprint[data-v-9e73065c],\n.editormd-html-preview .tag-pre.prettyprint[data-v-9e73065c]{padding:12px 18px;border:1px solid #ddd;white-space:-pre-wrap;word-wrap:break-word;box-sizing:border-box}.editormd-preview-container .tag-ol.linenums[data-v-9e73065c],\n.editormd-html-preview .tag-ol.linenums[data-v-9e73065c]{color:#999;padding-left:2.2em}.editormd-preview-container .tag-ol.linenums li[data-v-9e73065c],\n.editormd-html-preview .tag-ol.linenums li[data-v-9e73065c]{list-style-type:decimal}.editormd-preview-container .tag-ol.linenums li .tag-code[data-v-9e73065c],\n.editormd-html-preview .tag-ol.linenums li .tag-code[data-v-9e73065c]{border:none;background:none;padding:0}.editormd-preview-container .tag-code[data-v-9e73065c],\n.editormd-html-preview .tag-code[data-v-9e73065c]{border:0;\n\t/* display: inline-block; */\n\t/* padding-bottom: 0px; */background-color:#f6f6f6;color:#c7254e;padding:3px 4px;margin:3px}.editormd-preview-container .tag-pre[data-v-9e73065c],\n.editormd-html-preview .tag-pre[data-v-9e73065c]{border:0!important;background-color:#f6f6f6;line-height:1.7;font-size:.9em}.editormd-preview-container .tag-blockquote[data-v-9e73065c],\n.editormd-html-preview .tag-blockquote[data-v-9e73065c]{color:#777!important;font-style:normal!important;border-left:4px solid #d6dbdf;font-size:.9em;background:none repeat scroll 0 0 rgba(102,128,153,.05);margin:8px 0;padding:8px}.tag-blockquote .tag-p[data-v-9e73065c]{margin-bottom:0}.editormd-preview-container .tag-pre.prettyprint[data-v-9e73065c],\n.editormd-html-preview .tag-pre.prettyprint[data-v-9e73065c]{padding:12px 18px}@media (min-width:768px){.editormd-preview-container .tag-pre.prettyprint[data-v-9e73065c],\n\t.editormd-html-preview .tag-pre.prettyprint[data-v-9e73065c]{padding:12px 15px}}.page[data-v-9e73065c],\nuni-page-body[data-v-9e73065c]{min-height:100%}.title[data-v-9e73065c]{line-height:1.6;border-bottom:1px solid #efefef;padding:0 0 15px;margin-bottom:15px}.markdown-body[data-v-9e73065c]{-webkit-transition:all .5s;transition:all .5s;padding:15px 15px 60px;box-sizing:border-box}\n\n/* footer */.footer[data-v-9e73065c]{\n\t/* box-shadow: 0 0 5px #efefef; */border-top:1px solid #d5d5d5;position:fixed;height:48px;line-height:48px;bottom:0;left:0;width:100%;text-align:center;z-index:100;box-sizing:border-box;background-color:#fff;-webkit-transition:all .5s;transition:all .5s}.footer.hide[data-v-9e73065c]{bottom:-60px}.footer uni-image[data-v-9e73065c]{margin-top:11px;width:25px;height:25px}\n\n/* 抽屉 */.drawer[data-v-9e73065c]{width:80%;position:fixed;z-index:99;height:100%;overflow-y:scroll;top:0;-webkit-transition:all .5s;transition:all .5s;-webkit-overflow-scrolling:touch;background-color:#fff}.drawer .drawer-content[data-v-9e73065c]{width:100%;box-sizing:border-box;padding-bottom:60px}.drawer-left[data-v-9e73065c]{right:100%;border-right:%?1?% solid #efefef}.drawer-right[data-v-9e73065c]{left:100%;border-left:%?1?% solid #efefef}.drawer-left.show[data-v-9e73065c]{right:20%;border-right:1px solid #efefef}.drawer-right.show[data-v-9e73065c]{left:20%;border-left:1px solid #efefef}.more-setting[data-v-9e73065c]{position:absolute;width:100%}.setting-tips[data-v-9e73065c]{padding:8px 8px}.setting-item[data-v-9e73065c]{text-align:center;font-size:16px;line-height:16px;border:%?1?% solid #efefef;border-left:0;border-right:0;margin-bottom:22px}.more-setting .col[data-v-9e73065c]{padding:15px 0}.setting-btn[data-v-9e73065c]{margin-bottom:0;background-color:initial}.setting-btn uni-navigator[data-v-9e73065c]{border-left:%?1?% solid #efefef}.more-setting .col[data-v-9e73065c]:nth-of-type(2){border-right:%?1?% solid #efefef;border-left:%?1?% solid #efefef}.more-setting .setting-bg .col[data-v-9e73065c]:nth-of-type(2){border:0}.more-setting uni-image[data-v-9e73065c]{width:16px;height:16px}uni-slider[data-v-9e73065c]{width:100%}.bg-theme0[data-v-9e73065c]{background-color:#fff!important}.bg-theme1[data-v-9e73065c]{background-color:#e8e8e8!important}.bg-theme2[data-v-9e73065c]{background-color:#dfd6c6!important}.bg-theme3[data-v-9e73065c]{background-color:#d0bd8a!important}.bg-theme4[data-v-9e73065c]{background-color:#cfe7cf!important}.cont-box[data-v-9e73065c]{overflow-y:scroll}@media (min-width:768px){.drawer[data-v-9e73065c]{width:65%}.drawer-left.show[data-v-9e73065c]{right:35%}.drawer-right.show[data-v-9e73065c]{left:35%}}.markdown-body uni-image[data-v-9e73065c]{max-width:100%!important}.markdown-body uni-audio[data-v-9e73065c],\n.markdown-body uni-video[data-v-9e73065c]{max-width:100%}.is-horizon .header[data-v-9e73065c],\n.is-horizon .footer[data-v-9e73065c]{opacity:0}.is-horizon .header[data-v-9e73065c]:hover,\n.is-horizon .footer[data-v-9e73065c]:hover{opacity:1}',
          '',
        ]),
        (t.exports = e)
    },
    a6f1: function (t, e, a) {
      'use strict'
      var o = a('88b4'),
        n = a.n(o)
      n.a
    },
    b63c: function (t, e, a) {
      'use strict'
      a.r(e)
      var o = a('52b1'),
        n = a.n(o)
      for (var i in o)
        'default' !== i &&
          (function (t) {
            a.d(e, t, function () {
              return o[t]
            })
          })(i)
      e['default'] = n.a
    },
    c173: function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAwElEQVRYR+3Wyw3DIBAE0NlKk1SQK1SwHFOC00k6iTsh4sDZ+xnLioSvWJ7HCo8QXPzIxflYgP+ZgKp+RORbSnkwz415Aq21F4AngI2JMAPGrlV1E5EbE+ECnIFwA9iIEICJCANYiBSAgUgDsggKIIOgAaKIUwC993et9W6pbBpgtqQnfAApgGg4BZAJTwOy4SkAIzwMYIWHAMxwN4Ad7gLMK5n3Pz8qI3MPjEspgN3acEfBc90MsH7Q+94CrAn8ABCCjCHBT29gAAAAAElFTkSuQmCC'
    },
    de52: function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAChElEQVRYR+1WO2gVURScwacvBAsFUwiaKMYPilr5ayz8gYIiWiqCJiSNiCJh98JTNxrY3SREiBbiLwS7FKbwAwpa2Bi0EiRGMR8RY2MhSB67cV9GbngPlmesU7jb7bmzc+fOnLNcYp4fzvP+yARkDmQOZA5kDsw64Pv+KpJdknaQXAbg48zMTN/09PRNz/Nmqn/Xvu/vAeCR3FJeewfgkuu6r6qxAwMDC8bGxs4COA1gnaQfAIZKpdLFQqHwlZ7n1ebz+c8kl0sqApggubFMdMV13atpUt/3t5F8U65NSlpIss6+k9zlOM5QGh8EQTuAy7YmaRiAPWwtgMkoitYyCIJWALckPYjjuNnzvOmurq7VSZKMAEhc111MUhXSIAgGARyV1GSMuS/JcjSRvAPgoeu6x6sEFCUtALDeGDPR29ubLxaLdwGcBNBiP74O4DzJfY7jvEht9BTAwSRJVhQKhW+Vuu/7H0g2RFG0xIq1dc/zFuXz+V8APhljNlew3d3dDUmSTEh6bIw5nOI4QPIZgG76vt9D8oKkvcaYlynQE5KHkiSpt1mlhA1LaojjeGlaQE1NzU9J48aYTSlsPYAvAB65rnukUg/DcL+k5wB60hH0x3HcYkk7OzvXlEqlYZK/bQRVls5GQPKM4zh9NoIwDJsB3JY0aIw5VsGX16ZsBLlcbkNbW9u4jWBqauoeyROSWitNaBuvzjYhyXEAs6eQ1G6M8dICwjDcKen1XE0oabsx5m2V4GsACmW+9wAaSdZI+h7HcePsGHZ0dKzM5XI9lTGUNEKyP4qiG3ONYRAEuwFY4q0ASgAs8T/HcHR09BzJU7YR/xrD7EqWOZA5kDmQOfDfO/AHEktVhOhoKRUAAAAASUVORK5CYII='
    },
    e4c3: function (t, e, a) {
      'use strict'
      a('38cf'), Object.defineProperty(e, '__esModule', { value: !0 }), (e.default = void 0)
      var o = {
        data: function () {
          return { audio: uni.createInnerAudioContext(), current: 0, duration: 0, paused: !0, loading: !1, seek: !1 }
        },
        props: {
          title: { type: String, default: '未知音频' },
          src: { type: String, default: '' },
          autoplay: { type: Boolean, default: !1 },
          continue: { type: Boolean, default: !0 },
          control: { type: Boolean, default: !0 },
          color: { type: String, default: '#169af3' },
        },
        methods: {
          prev: function () {
            this.$emit('prev')
          },
          next: function () {
            this.$emit('next')
          },
          format: function (t) {
            return (
              '0'.repeat(2 - String(Math.floor(t / 60)).length) +
              Math.floor(t / 60) +
              ':' +
              '0'.repeat(2 - String(Math.floor(t % 60)).length) +
              Math.floor(t % 60)
            )
          },
          play: function () {
            this.audio.play(), (this.loading = !0)
          },
        },
        created: function () {
          var t = this
          this.src && ((this.audio.src = this.src), this.autoplay && this.play()),
            (this.audio.obeyMuteSwitch = !1),
            this.audio.onTimeUpdate(function () {
              t.seek || (t.current = t.audio.currentTime), t.duration || (t.duration = t.audio.duration)
            }),
            this.audio.onPlay(function () {
              ;(t.paused = !1), (t.loading = !1)
            }),
            this.audio.onPause(function () {
              t.paused = !0
            }),
            this.audio.onEnded(function () {
              t.continue ? t.next() : ((t.paused = !0), (t.current = 0))
            }),
            this.audio.onSeeked(function () {
              t.seek = !1
            })
        },
        beforeDestroy: function () {
          this.audio.destroy()
        },
        watch: {
          src: function (t, e) {
            ;(this.audio.src = t), (this.current = 0), (this.duration = 0), (e || this.autoplay) && this.play()
          },
        },
      }
      e.default = o
    },
    e4ce: function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAApklEQVRYR+1USQrDMAyUvtPQvKaPsQ65FWy/pb/J9p0JKWkJBIqduFgH6aSDNRqPmGGqXFx5PxmBgwIxxgeA53oaAJ2IvP55pgMB7/3EzLeNwCwizdqHEEYievdnC8AX74ORQ6AnovvZ5dvc4Jxr9xj6TnDxh9njZkNTQJ8CloQ/otiSMDvlUgb0uSCFdck3poA+BSwJLQlLejwFS58LUliXfFNdgQXmbpkh7DJNzgAAAABJRU5ErkJggg=='
    },
    eea3: function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAC6UlEQVRYR7VXS3LaQBDtJ8qjrXMC4xMEn8DcIMkJYjaMtIMTBN+AnTRsgF12ISeIvMsu3MDcIN5aVUyn2jVyyUITBJJnQ9VomH79e/0G5FlJkgwBfAMwIKJL37lj+8ycMfN9HMdZ3VnUbRpjJkQ0s9bKbxbH8e6YoeJ7kiR3QRDMiGiilMryPL9zdw3q7jkAkCRJPwiCRwA34/F429RwcS5N0w2ATGs9L+3NATxprQXYm3UAwBgzY+bLKIrE+5OXMSaz1s7KIZc75aJGAJwH27rDTdAYY3bM/JeInkrn+wDWjQCIB5L3cwGkafrbWvsdQDV9u0Y10BbAYrEYXFxc7EajUTkC3uDV1cBZEZDiBfAJwEvLMvM2DMOHY0BaA3CGl9I1RLQhopeWZebPAK6IaK61vveFoBUACTcz/2LmdRiGs6q3jsykHf9EUTRqRERNa2C5XF7mef5orZ3GcbzyeSjnnp+fMwCbTrvA9fZQaz0sG68rQkduW2vtARuenQLpd6FbrbXk/XXVEZF8FH5h5k01Wm0AsNb69f/iZa/Xu2LmOTOvgiDYjsfjhwKZj2E7A5CmqfC9FKVMz51wf5mSfSlrBUAp9aFa+b4U+OZBGwAydFbVnEquZRxXJ2maplsAs2rNnA1A5r4IljAMb46xndMXUrD9JuO4MRW7yclKqZEPREFW1tovdaro7AiIJ45khIQ+SktGUfSzpIxE2HwloqkoKx9ZtQJQlmEAJgAEiIxhCbUMpbXrBK+k6wRA2WtnnHwitNMaqON/GUC9Xu+pqZ48OQLGGBmzO5+BpsOsAH8SAEcmhVi9q/a0XPpuAIwxUu3i/XC/3w+CIJjXVfe7ACgbL0Lv+lvY8E2LdQrAiY4lM1+HYTiskk0BQihBaz3tNAVKqXme5yK3UGe8KKICBDP/ENnVSQRE0QK4PWa8AFHILtF+AK5PeVf4uuBWWEwpNTk2aKogHBveN33YdPo2LAnQVflx+r8n3T8KzV8/MXLpHAAAAABJRU5ErkJggg=='
    },
    f327: function (t, e, a) {
      'use strict'
      var o
      a.d(e, 'b', function () {
        return n
      }),
        a.d(e, 'c', function () {
          return i
        }),
        a.d(e, 'a', function () {
          return o
        })
      var n = function () {
          var t = this,
            e = t.$createElement,
            a = t._self._c || e
          return a(
            'v-uni-view',
            { staticClass: 'imt-audio' },
            [
              a(
                'v-uni-view',
                { staticClass: 'audio-box' },
                [
                  a(
                    'v-uni-view',
                    { staticClass: 'audio-poster' },
                    [
                      a(
                        'v-uni-view',
                        { staticClass: 'audio-control-wrapper', style: t.color },
                        [
                          a(
                            'v-uni-view',
                            {
                              staticClass: 'audio-control audio-control-switch',
                              class: { audioLoading: t.loading },
                              style: { borderColor: t.color },
                              on: {
                                click: function (e) {
                                  ;(arguments[0] = e = t.$handleEvent(e)), t.audio.paused ? t.play() : t.audio.pause()
                                },
                              },
                            },
                            [t._v(t._s(t.loading ? '' : t.paused ? '' : ''))]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  a('v-uni-view', { staticClass: 'audio-title' }, [t._v(t._s(t.title))]),
                  a(
                    'v-uni-view',
                    { staticClass: 'audio-wrapper' },
                    [
                      a('v-uni-view', { staticClass: 'audio-number' }, [t._v(t._s(t.format(t.current)))]),
                      a('v-uni-slider', {
                        staticClass: 'audio-slider',
                        attrs: { activeColor: t.color, 'block-size': '16', value: t.current, max: t.duration },
                        on: {
                          changing: function (e) {
                            ;(arguments[0] = e = t.$handleEvent(e)), (t.seek = !0), (t.current = e.detail.value)
                          },
                          change: function (e) {
                            ;(arguments[0] = e = t.$handleEvent(e)), t.audio.seek(e.detail.value)
                          },
                        },
                      }),
                      a('v-uni-view', { staticClass: 'audio-number text-right' }, [t._v(t._s(t.format(t.duration)))]),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          )
        },
        i = []
    },
  },
])
