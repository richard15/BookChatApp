var http = require('http')

http
  .createServer((req, res) => {
    console.log(req)
    res.write('Server is running!')
    res.end()
  })
  .listen(8082)
